# Ejemplo de aplicación tray Qt5 con python

## Entorno de desarrollo

Crear un entorno virtual

    # virtualenv venv -p python3
    # . venv/bin/activate
    # pip install -r requirements.txt
    # python echidnalink.py

## Construcción de ejecutable para distribución

    # pyinstaller --onefile --windowed  echidnalink.py

Se genera un directorio `dist` en el que se encuentra el ejecutable

### Sobre el fichero echidna_logo.qrc

Para que el logo `echidna.png` pueda incorporarse en el ejecutable se hace
lo siguiente:

1. Se crea el archivo echidna_logo.qrc

```xml
<!DOCTYPE RCC><RCC version="1.0">
<qresource prefix="/">
   <file>echidna.png</file>
</qresource>
</RCC>
```

Se ejecuta la instrucción

```bash
# pyrcc5 "echidna_logo.qrc" -o echidna_logo.py
```

O lo que es lo mismo, se ejecuta el script:
```bash
# generate-echidna-logo.sh
```
En su interior se encuentra la misma orden anterior.

Esto genera un archivo `echidna_logo.py` con el logo codificado, de manera
que en lugar de usar en el archivo `echidnalink.py` la imagen `echidna.logo`
para crear el icono:

```python
icon = QIcon("echidna.png")
```

Usamos la siguiente notación (dos puntos delante del nombre del archivo), 
para que se use la representación generada en `echidna_logo.py`:

```python
icon = QIcon(":echidna.png")
```

Ahora, al crear el ejecutable con `pyinstaller` se incorpora la imagen del 
icono sin problemas.

Nota: Esto no es necesario hacerlo de nuevo, ya que se ha incorporado al control
de versiones el archivo `echidna_logo.py`, el único que es necesario para que 
funcione todo este rollo.